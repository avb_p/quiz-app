import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QuizService } from '../../providers/quiz-service';
import { QuizEditorPage } from '../../pages/quiz-editor/quiz-editor';
import { QuizOverviewPage } from '../../pages/quiz-overview/quiz-overview';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers : [QuizService]
})
export class HomePage {

  public quizHeaders;
  public questions;


  constructor(public navCtrl: NavController, private quizService:QuizService) {
    this.loadHeaders();
  }

loadHeaders(){
  this.quizService.load()
    .then(data => {
      this.quizHeaders = data;
      console.log(data);
    })
}

loadQuestions(){
  this.quizService.loadQuestions()
    .then(data => {
      this.questions = data;
    });
}


openEditorPage(){
  this.navCtrl.push(QuizEditorPage);
}

openQuizOverviewPage(){
  this.navCtrl.push(QuizOverviewPage)
}

};
 