import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
/*
  Generated class for the QuizEditor page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-quiz-editor',
  templateUrl: 'quiz-editor.html',
})
export class QuizEditorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

openHomePage(){
  this.navCtrl.push(HomePage)
}


}
