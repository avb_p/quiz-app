import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { QuizService } from '../../providers/quiz-service';

/*
  Generated class for the QuizOverview page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-quiz-overview',
  templateUrl: 'quiz-overview.html',
  providers : [QuizService]
})
export class QuizOverviewPage {

	public questions;
	public quizHeaders;
	

  	constructor(public navCtrl: NavController, public navParams: NavParams, private quizService:QuizService) {
 	this.loadQuestions();
 	this.loadHeaders();
  	}


	openHomePage(){
	this.navCtrl.push(HomePage)
	};

	loadQuestions(){
	this.quizService.loadQuestions()
		.then(data => {
			this.questions = data;
		});
	};

	loadHeaders(){
  	this.quizService.load()
    .then(data => {
      this.quizHeaders = data;
      console.log(data);
    })
}

}
